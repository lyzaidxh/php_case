<?php

use Swoole\Http\Request;
use Swoole\Http\Response;



$subject_connnection_map = array();

error_reporting(E_ALL);

\Swoole\Coroutine\run(
    function () {
        $server = new Swoole\Coroutine\Http\Server('0.0.0.0', 9501, false);
//        $server = new Swoole\Coroutine\Http\Server('0.0.0.0', 9509, true);
//        $server->set(
//            [
//                'ssl_key_file' => __DIR__ . '/config/ssl.key',
//                'ssl_cert_file' => __DIR__ . '/config/ssl.crt',
//            ]
//        );

        $server->handle(
            '/',
            function (Request $req, Response $resp) {
                //websocket
                if (isset($req->header['upgrade']) and $req->header['upgrade'] == 'websocket') {
                    $resp->upgrade();
                    $resp->subjects = array();
                    while (true) {
                        $frame = $resp->recv();
                        if (empty($frame)) {
                            break;
                        }
                        $data = json_decode($frame->data, true);
                        var_dump($data);
                        switch ($data['cmd']) {
                            // 订阅主题
                            case 'subscribe':
                                $subject = $data['subject'];
                                subscribe($subject, $resp);
                                break;
                            // 向某个主题发布消息
                            case 'publish':
                                $subject = $data['subject'];
                                $event = $data['event'];
                                $data = $data['data'];
                                publish($subject, $event, $data, $resp);
                                break;
                        }
                    }
                    destry_connection($resp);
                    return;
                }
          
            }
        );

        $server->start();
    }
);


// 订阅
function subscribe($subject, $connection)
{
    global $subject_connnection_map;
    $connection->subjects[$subject] = $subject;
    $subject_connnection_map[$subject][$connection->fd] = $connection;
}

// 取消订阅
function unsubscribe($subject, $connection)
{
    global $subject_connnection_map;
    unset($subject_connnection_map[$subject][$connection->fd]);
}

// 向某个主题发布事件
function publish($subject, $event, $data, $exclude)
{
    global $subject_connnection_map;
    if (empty($subject_connnection_map[$subject])) {
        return;
    }
    foreach ($subject_connnection_map[$subject] as $connection) {
        if ($exclude == $connection) {
            continue;
        }
        $connection->push(
            json_encode(
                array(
                    'cmd' => 'publish',
                    'event' => $event,
                    'data' => $data
                )
            )
        );
    }
}

// 清理主题映射数组
function destry_connection($connection)
{
    foreach ($connection->subjects as $subject) {
        unsubscribe($subject, $connection);
    }
}

function exec_php_file($file)
{
    \ob_start();
    // Try to include php file.
    try {
        include $file;
    } catch (\Exception $e) {
        echo $e;
    }
    return \ob_get_clean();
}


