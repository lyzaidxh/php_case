<?php
require 'vendor/autoload.php';
use QL\QueryList;
$str = file_get_contents('a.txt');
$str = explode(PHP_EOL, $str);

foreach ($str as $v) {
    $rules = array(
        'company' =>
            array(
                0 => '.company-name',
                1 => 'text',
            ),
        'primary' =>
            array(
                0 => '.text-primary',
                1 => 'text',
            ),
        'email' =>
            array(
                0 => '.email',
                1 => 'text',
            ),
        'phone' =>
            array(
                0 => '.phone',
                1 => 'text',
            ),
        'address' =>
            array(
                0 => '.address',
                1 => 'text',
            ),
    );
//    $list = QueryList::get($v)->rules($rules)->range('')->queryData();
//    $item=$list[0]['company'] . ',' . $list[0]['primary'] . ',' . $list[0]['email'] . ',' . $list[0]['phone'] . ',' . $list[0]['address'];
//    file_put_contents('b.txt',$item.PHP_EOL,FILE_APPEND);

    $list = QueryList::get($v,[
    ],[
        'headers' => [
            'User-Agent' => 'Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3941.4 Mobile Safari/537.36',
            'Accept'     => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'X-Foo'      => ['Bar', 'Baz'],
            // 携带cookie
//            'Host'=>'m.qichacha.com',
        'Cookie'=>'zg_did=%7B%22did%22%3A%20%2216e1571419d14e-04c935f473ff86-78462040-15f900-16e1571419e1b5%22%7D; UM_distinctid=16e1571436b20f-07248d9ca80b9f-78462040-15f900-16e1571436c2ae; acw_tc=7909f42715741600998447527e5d1117a60c4e7e4b4371c889a4bf8b94; QCCSESSID=7njjdb5juak4u9i392qu1id6u4; PHPSESSID=hltvrsf0d70levr2kvfmrgf0i2; Hm_lvt_3456bee468c83cc63fb5147f119f1075=1574218190,1574229826,1574230403,1574231399; CNZZDATA1254842228=103787389-1574157308-%7C1574228813; acw_sc__v2=5dd4e58c67db4159893aa693f7fa454fb91ee8db; acw_sc__v3=5dd4e58eb635b8d02b2da2f8fc63adaab3febe24; zg_de1d1a35bfa24ce29bbf2c7eb17e6c4f=%7B%22sid%22%3A%201574225897840%2C%22updated%22%3A%201574233524157%2C%22info%22%3A%201574040919750%2C%22superProperty%22%3A%20%22%7B%7D%22%2C%22platform%22%3A%20%22%7B%7D%22%2C%22utm%22%3A%20%22%7B%7D%22%2C%22referrerDomain%22%3A%20%22%22%2C%22zs%22%3A%200%2C%22sc%22%3A%200%2C%22cuid%22%3A%20%22bb90463d5bdf910677764fe886b87ebb%22%7D; Hm_lpvt_3456bee468c83cc63fb5147f119f1075=1574233524'
        ]
    ])->rules($rules)->range('')->queryData();
    $item=$list[0]['company'] . ',' . $list[0]['primary'] . ',' . $list[0]['email'] . ',' . $list[0]['phone'] . ',' . $list[0]['address'];
    file_put_contents('b.txt',$item.PHP_EOL,FILE_APPEND);
}



