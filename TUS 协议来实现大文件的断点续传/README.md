第一步，加载依赖:
composer require ankitpokhrel/tus-php

第二步：
创建一个服务端 server.php
require __DIR__ . '/vendor/autoload.php';
$server   = new \TusPhp\Tus\Server('redis');
$response = $server->serve();
$response->send();
exit(0); // 退出当前 PHP 进程

第三步：Nginx配置文件配置转发
location /files {
    try_files $uri $uri/ /server.php?$query_string;
}

413错误情况：
在nginx.conf文件 http{}中加入client_max_body_size 100m;